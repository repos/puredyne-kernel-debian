#!/usr/bin/make -f
DATE      := $(shell date +%Y%m%d)
M         := $(CURDIR)
CFLAGS     = -Wall -g
# Include dpatch (don't fail, because we do not need it for m-a)
-include /usr/share/dpatch/dpatch.make
include debian/conf.mk

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -O2
endif


# Name of the source package
psource:=aufs-source
# The short upstream name, used for the module source directory
sname:=aufs

### KERNEL SETUP
# prefix of the target package name
PACKAGE = aufs-modules
MA_DIR ?= /usr/share/modass
-include $(MA_DIR)/include/generic.make
-include $(MA_DIR)/include/common-rules.make
kdist_config: prep-deb-files

kdist_clean:
	$(MAKE) -C $(KSRC) M=$(CURDIR) clean

# Create the directories to
#
### end  KERNEL SETUP

build-arch: build-arch-stamp

build-arch-stamp:
	dh_testdir
	$(MAKE) -C util
	touch $@

binary-modules:
	dh_testroot
	dh_clean -k
	# Build the module
	$(MAKE) -C $(KSRC) M=$(CURDIR) modules
	# Install the module
	install -D -m 0644 aufs.ko debian/$(PKGNAME)/lib/modules/$(KVERS)/kernel/fs/aufs/aufs.ko

	dh_installdocs
	dh_installchangelogs
	dh_compress
	dh_fixperms
	dh_installmodules
	dh_installdeb
	dh_gencontrol -- -v$(VERSION)
	dh_md5sums
	dh_builddeb --destdir=$(DEB_DESTDIR)
	dh_clean -k

build: patch build-arch

clean: unpatch
	dh_testdir
	#dh_testroot
	rm -f build-arch-stamp
	$(MAKE) -C util clean

	dh_clean

install: DH_OPTIONS=
install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs
	dh_installkpatches
	# Create the directories to install the source into
	dh_installdirs -p$(psource) usr/src/modules/$(sname)/debian \
				    usr/src/modules/$(sname)/include
	dh_installdirs -A -paufs-tools  usr/sbin

	# Copy only the driver source to the proper location
	cp -r fs/aufs/*        debian/$(psource)/usr/src/modules/$(sname)/
	cp -r include/*        debian/$(psource)/usr/src/modules/$(sname)/include
	cd debian/$(psource)/usr/src/modules/$(sname)/ && mv Makefile Makefile.upstream
	cp debian/modules.mk   debian/$(psource)/usr/src/modules/$(sname)/Makefile
	cp debian/*modules.in* debian/$(psource)/usr/src/modules/$(sname)/debian
	cp debian/rules debian/changelog debian/copyright debian/compat debian/conf.mk \
		debian/$(psource)/usr/src/modules/$(sname)/debian/
	cd debian/$(psource)/usr/src && tar c modules | bzip2 -9 > $(sname).tar.bz2 && rm -rf modules

	for i in mount.aufs umount.aufs auplink aulchown; do \
		install -m 755 -p $(CURDIR)/util/$$i debian/aufs-tools/usr/sbin || exit 1; \
	done;
	dh_install

upstream:
	# Needs: cvs
	# Getting sources
	cd .. && \
	cvs -z3 -d:pserver:anonymous@aufs.cvs.sourceforge.net:/cvsroot/aufs co aufs && \
	mv aufs aufs-0+$(DATE)
	# Removing unused files
	find ../aufs-0+$(DATE) -type d -name CVS -exec rm -rf {} \; || exit 0
	find ../aufs-0+$(DATE) -type f -name .cvsignore -exec rm -f {} \; || exit 0
	# Creating tarball
	tar cfz ../aufs_0+$(DATE).orig.tar.gz ../aufs-0+$(DATE)

binary-indep: build install
	dh_testdir -i
	dh_testroot -i
	dh_installchangelogs -i $(CURDIR)/History
	dh_installdocs -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary-arch: build install
	dh_testdir -s
	dh_testroot -s
	dh_installdocs -s
	dh_installman -s
	dh_installexamples -s
	dh_installchangelogs -s $(CURDIR)/History
	dh_strip -s
	dh_link -s
	dh_compress -s
	dh_fixperms -s
	dh_installdeb -s
	dh_shlibdeps -s
	dh_gencontrol -s
	dh_md5sums -s
	dh_builddeb -s

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install binary-modules kdist kdist_config kdist_image kdist_clean
