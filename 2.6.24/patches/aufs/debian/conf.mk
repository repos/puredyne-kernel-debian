#!/usr/bin/make -f
# Configuration for aufs in Debian
define exported
ifneq (,$(shell grep '^.*[[:space:]]$(1)[[:space:]]vmlinux[[:space:]]EXPORT_SYMBOL' $(KSRC)/Module.symvers))
export $(2)=y
export EXTRA_CFLAGS += -D$(2)
endif
endef

# Configuration for aufs in Debian
export CONFIG_AUFS                = m
export CONFIG_AUFS_BRANCH_MAX_127 = y
export CONFIG_AUFS_RR_SQUASHFS    = y
export EXTRA_CFLAGS               = -I $(M)/include \
                                    -DCONFIG_AUFS_BRANCH_MAX_127  -UCONFIG_AUFS \
                                    -DCONFIG_AUFS_MODULE \
                                    -DCONFIG_AUFS_RR_SQUASHFS

# Disable sysaufs on ARM, as there is no cmpxchg
ifneq ($(shell dpkg-architecture -qDEB_BUILD_ARCH),arm)
export CONFIG_AUFS_SYSAUFS  = y
export EXTRA_CFLAGS        += -DCONFIG_AUFS_SYSAUFS
endif

ifneq (,$(KSRC))
$(eval $(call exported,ksize,CONFIG_AUFS_KSIZE_PATCH)) #ksize
$(eval $(call exported,__lookup_hash,CONFIG_AUFS_LHASH_PATCH)) #lhash
$(eval $(call exported,sysfs_get_dentry,CONFIG_AUFS_SYSFS_GET_DENTRY_PATCH)) #sysfs_get_dentry
$(eval $(call exported,put_filp,CONFIG_AUFS_PUT_FILP_PATCH)) #put_filp
$(eval $(call exported,security_inode_permission,CONFIG_AUFS_SEC_PERM_PATCH)) #sysfs_get_dentry
endif

# If lhash and put_filp are not available, activate FAKE_DM.
ifndef CONFIG_AUFS_PUT_FILP_PATCH
ifndef CONFIG_AUFS_LHASH_PATCH
export CONFIG_AUFS_FAKE_DM  = y
export EXTRA_CFLAGS        += -DCONFIG_AUFS_FAKE_DM
endif
endif
