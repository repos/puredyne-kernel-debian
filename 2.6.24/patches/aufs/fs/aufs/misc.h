/*
 * Copyright (C) 2005, 2006, 2007 Junjiro Okajima
 *
 * This program, aufs is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/* $Id: misc.h,v 1.39 2008/01/21 04:57:48 sfjro Exp $ */

#ifndef __AUFS_MISC_H__
#define __AUFS_MISC_H__

#ifdef __KERNEL__

#include <linux/fs.h>
#include <linux/namei.h>
#include <linux/sched.h>
#include <linux/version.h>
#include <linux/aufs_type.h>


/* ---------------------------------------------------------------------- */

/*
 * support for RT patch (testing).
 * it uses 'standard compat_rw_semaphore' instead of 'realtime rw_semaphore.'
 * sigh, wrapper for wrapper...
 */

#ifndef CONFIG_PREEMPT_RT

#define au_rw_semaphore		rw_semaphore
#define au_init_rwsem		init_rwsem
#define au_down_read		down_read
#define au_down_read_nested	down_read_nested
#define au_down_read_trylock	down_read_trylock
#define au_up_read		up_read
#define au_downgrade_write	downgrade_write
#define au_down_write		down_write
#define au_down_write_nested	down_write_nested
#define au_down_write_trylock	down_write_trylock
#define au_up_write		up_write

static inline int au_rt_s_files_lock(struct super_block *sb)
{
	/* nothing */
	return 0;
}

static inline void au_rt_s_files_unlock(struct super_block *sb, int idx)
{
	/* nothing */
}

#define au_rt_s_files_loop(pos, sb) \
	list_for_each_entry(pos, &(sb)->s_files, f_u.fu_list)
#define au_rt_s_files_loop_break(pos)	do {} while (0)

static inline unsigned long au_mapping_nrpages(struct address_space *mapping)
{
	return mapping->nrpages;
}

#else /* CONFIG_PREEMPT_RT */

#define au_rw_semaphore		compat_rw_semaphore
#define au_init_rwsem		compat_init_rwsem
#define au_down_read		compat_down_read
#define au_down_read_nested	compat_down_read_nested
#define au_down_read_trylock	compat_down_read_trylock
#define au_up_read		compat_up_read
#define au_downgrade_write	compat_downgrade_write
#define au_down_write		compat_down_write
#define au_down_write_nested	compat_down_write_nested
#define au_down_write_trylock	compat_down_write_trylock
#define au_up_write		compat_up_write

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 23)

static inline int au_rt_s_files_lock(struct super_block *sb)
{
	int idx;
	idx = qrcu_read_lock(&sb->s_qrcu);
	percpu_list_fold(&sb->s_files);
	return idx;
}

static inline void au_rt_s_files_unlock(struct super_block *sb, int idx)
{
	qrcu_read_unlock(&sb->s_qrcu, idx);
}

#define au_rt_s_files_loop(pos, sb) \
	lock_list_for_each_entry(pos, percpu_list_head(&(sb)->s_files), \
				 f_u.fu_llist)

#define au_rt_s_files_loop_break(pos) \
	lock_list_for_each_entry_stop(pos, f_u.fu_llist);

static inline unsigned long au_mapping_nrpages(struct address_space *mapping)
{
	return mapping_nrpages(mapping);
}

#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 20)

static inline int au_rt_s_files_lock(struct super_block *sb)
{
	//barrier_lock(&sb->s_barrier);
	filevec_add_drain_all();
	return 0;
}

static inline void au_rt_s_files_unlock(struct super_block *sb, int idx)
{
	//barrier_unlock(&sb->s_barrier);
}

#define au_rt_s_files_loop(pos, sb) \
	lock_list_for_each_entry(pos, &(sb)->s_files, f_u.fu_llist)

#define au_rt_s_files_loop_break(pos) \
	lock_list_for_each_entry_stop(pos, f_u.fu_llist);

static inline unsigned long au_mapping_nrpages(struct address_space *mapping)
{
	return mapping->nrpages;
}

#else

static inline int au_rt_s_files_lock(struct super_block *sb)
{
	/* nothing */
	return 0;
}

static inline void au_rt_s_files_unlock(struct super_block *sb, int idx)
{
	/* nothing */
}

#define au_rt_s_files_loop(pos, sb) \
	list_for_each_entry(pos, &(sb)->s_files, f_u.fu_list)
#define au_rt_s_files_loop_break(pos)	do {} while (0)

static inline unsigned long au_mapping_nrpages(struct address_space *mapping)
{
	return mapping->nrpages;
}

#endif /* LINUX_VERSION_CODE */
#endif /* CONFIG_PREEMPT_RT */

/* ---------------------------------------------------------------------- */

#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 18)
#define I_MUTEX_QUOTA			0
#define lockdep_off()			do {} while (0)
#define lockdep_on()			do {} while (0)
#define mutex_lock_nested(mtx, lsc)	mutex_lock(mtx)
#define down_write_nested(rw, lsc)	down_write(rw)
#define down_read_nested(rw, lsc)	down_read(rw)
#endif

/* ---------------------------------------------------------------------- */

typedef unsigned int au_gen_t;
/* see linux/include/linux/jiffies.h */
#define AuGenYounger(a, b)	((int)(b) - (int)(a) < 0)
#define AuGenOlder(a, b)	AufsGenYounger(b, a)

/* ---------------------------------------------------------------------- */

struct aufs_rwsem {
	struct au_rw_semaphore	rwsem;
#ifdef CONFIG_AUFS_DEBUG
	atomic_t		rcnt;
#endif
};

#ifdef CONFIG_AUFS_DEBUG
#define DbgRcntInit(rw) do { \
	atomic_set(&(rw)->rcnt, 0); \
	smp_mb(); \
} while (0)

#define DbgRcntInc(rw)		atomic_inc_return(&(rw)->rcnt)
#define DbgRcntDec(rw)		WARN_ON(atomic_dec_return(&(rw)->rcnt) < 0)
#else
#define DbgRcntInit(rw)		do {} while (0)
#define DbgRcntInc(rw)		do {} while (0)
#define DbgRcntDec(rw)		do {} while (0)
#endif /* CONFIG_AUFS_DEBUG */

static inline void rw_init_nolock(struct aufs_rwsem *rw)
{
	DbgRcntInit(rw);
	au_init_rwsem(&rw->rwsem);
}

static inline void rw_init_wlock(struct aufs_rwsem *rw)
{
	rw_init_nolock(rw);
	au_down_write(&rw->rwsem);
}

static inline void rw_init_wlock_nested(struct aufs_rwsem *rw, unsigned int lsc)
{
	rw_init_nolock(rw);
	au_down_write_nested(&rw->rwsem, lsc);
}

static inline void rw_read_lock(struct aufs_rwsem *rw)
{
	au_down_read(&rw->rwsem);
	DbgRcntInc(rw);
}

static inline void rw_read_lock_nested(struct aufs_rwsem *rw, unsigned int lsc)
{
	au_down_read_nested(&rw->rwsem, lsc);
	DbgRcntInc(rw);
}

static inline void rw_read_unlock(struct aufs_rwsem *rw)
{
	DbgRcntDec(rw);
	au_up_read(&rw->rwsem);
}

static inline void rw_dgrade_lock(struct aufs_rwsem *rw)
{
	DbgRcntInc(rw);
	au_downgrade_write(&rw->rwsem);
}

static inline void rw_write_lock(struct aufs_rwsem *rw)
{
	au_down_write(&rw->rwsem);
}

static inline void rw_write_lock_nested(struct aufs_rwsem *rw, unsigned int lsc)
{
	au_down_write_nested(&rw->rwsem, lsc);
}

static inline void rw_write_unlock(struct aufs_rwsem *rw)
{
	au_up_write(&rw->rwsem);
}

/* why is not _nested version defined */
static inline int rw_read_trylock(struct aufs_rwsem *rw)
{
	int ret = au_down_read_trylock(&rw->rwsem);
	if (ret)
		DbgRcntInc(rw);
	return ret;
}

static inline int rw_write_trylock(struct aufs_rwsem *rw)
{
	return au_down_write_trylock(&rw->rwsem);
}

#undef DbgRcntInit
#undef DbgRcntInc
#undef DbgRcntDec

/* to debug easier, do not make them inlined functions */
#define RwMustNoWaiters(rw)	AuDebugOn(!list_empty(&(rw)->rwsem.wait_list))
#define RwMustAnyLock(rw)	AuDebugOn(au_down_write_trylock(&(rw)->rwsem))
#ifdef CONFIG_AUFS_DEBUG
#define RwMustReadLock(rw) do { \
	RwMustAnyLock(rw); \
	AuDebugOn(!atomic_read(&(rw)->rcnt)); \
} while (0)

#define RwMustWriteLock(rw) do { \
	RwMustAnyLock(rw); \
	AuDebugOn(atomic_read(&(rw)->rcnt)); \
} while (0)
#else
#define RwMustReadLock(rw)	RwMustAnyLock(rw)
#define RwMustWriteLock(rw)	RwMustAnyLock(rw)
#endif /* CONFIG_AUFS_DEBUG */

#define SimpleLockRwsemFuncs(prefix, param, rwsem) \
static inline void prefix##_read_lock(param) \
{ rw_read_lock(&(rwsem)); } \
static inline void prefix##_write_lock(param) \
{ rw_write_lock(&(rwsem)); } \
static inline int prefix##_read_trylock(param) \
{ return rw_read_trylock(&(rwsem)); } \
static inline int prefix##_write_trylock(param) \
{ return rw_write_trylock(&(rwsem)); }
//static inline void prefix##_read_trylock_nested(param, lsc)
//{rw_read_trylock_nested(&(rwsem, lsc));}
//static inline void prefix##_write_trylock_nestd(param, lsc)
//{rw_write_trylock_nested(&(rwsem), nested);}

#define SimpleUnlockRwsemFuncs(prefix, param, rwsem) \
static inline void prefix##_read_unlock(param) \
{ rw_read_unlock(&(rwsem)); } \
static inline void prefix##_write_unlock(param) \
{ rw_write_unlock(&(rwsem)); } \
static inline void prefix##_downgrade_lock(param) \
{ rw_dgrade_lock(&(rwsem)); }

#define SimpleRwsemFuncs(prefix, param, rwsem) \
	SimpleLockRwsemFuncs(prefix, param, rwsem) \
	SimpleUnlockRwsemFuncs(prefix, param, rwsem)

/* ---------------------------------------------------------------------- */

void *au_kzrealloc(void *p, unsigned int nused, unsigned int new_sz, gfp_t gfp);

struct aufs_sbinfo;
struct nameidata *au_dup_nd(struct aufs_sbinfo *sbinfo, struct nameidata *dst,
			    struct nameidata *src);
struct nameidata *au_fake_dm(struct nameidata *fake_nd, struct nameidata *nd,
			     struct super_block *sb, aufs_bindex_t bindex);
void au_fake_dm_release(struct nameidata *fake_nd);
int au_h_create(struct inode *h_dir, struct dentry *h_dentry, int mode,
		int dlgt, struct nameidata *nd, struct vfsmount *nfsmnt);

int au_copy_file(struct file *dst, struct file *src, loff_t len,
		 struct super_block *sb);
int au_test_ro(struct super_block *sb, aufs_bindex_t bindex,
	       struct inode *inode);
int au_test_perm(struct inode *h_inode, int mask, int dlgt);

#endif /* __KERNEL__ */
#endif /* __AUFS_MISC_H__ */
