/*
 * Copyright (C) 2005, 2006, 2007 Junjiro Okajima
 *
 * This program, aufs is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/* $Id: iinfo.c,v 1.45 2008/01/21 04:57:48 sfjro Exp $ */

#include "aufs.h"

struct aufs_iinfo *itoii(struct inode *inode)
{
	struct aufs_iinfo *iinfo;

	iinfo = &(container_of(inode, struct aufs_icntnr, vfs_inode)->iinfo);
	/* bad_inode case */
	if (unlikely(!iinfo->ii_hinode))
		return NULL;
	AuDebugOn(!iinfo->ii_hinode
		  /* || stosi(inode->i_sb)->si_bend < iinfo->ii_bend */
		  || iinfo->ii_bend < iinfo->ii_bstart);
	return iinfo;
}

aufs_bindex_t ibstart(struct inode *inode)
{
	IiMustAnyLock(inode);
	return itoii(inode)->ii_bstart;
}

aufs_bindex_t ibend(struct inode *inode)
{
	IiMustAnyLock(inode);
	return itoii(inode)->ii_bend;
}

struct aufs_vdir *ivdir(struct inode *inode)
{
	IiMustAnyLock(inode);
	AuDebugOn(!S_ISDIR(inode->i_mode));
	return itoii(inode)->ii_vdir;
}

struct dentry *au_hi_wh(struct inode *inode, aufs_bindex_t bindex)
{
	struct aufs_hinode *hinode;

	IiMustAnyLock(inode);

	hinode = itoii(inode)->ii_hinode + bindex;
	return hinode->hi_whdentry;
}

struct inode *au_h_iptr_i(struct inode *inode, aufs_bindex_t bindex)
{
	struct inode *hidden_inode;

	IiMustAnyLock(inode);
	AuDebugOn(bindex < 0 || ibend(inode) < bindex);
	hidden_inode = itoii(inode)->ii_hinode[0 + bindex].hi_inode;
	AuDebugOn(hidden_inode && atomic_read(&hidden_inode->i_count) <= 0);
	return hidden_inode;
}

struct inode *au_h_iptr(struct inode *inode)
{
	return au_h_iptr_i(inode, ibstart(inode));
}

aufs_bindex_t itoid_index(struct inode *inode, aufs_bindex_t bindex)
{
	IiMustAnyLock(inode);
	AuDebugOn(bindex < 0
		  || ibend(inode) < bindex
		  || !itoii(inode)->ii_hinode[0 + bindex].hi_inode);
	return itoii(inode)->ii_hinode[0 + bindex].hi_id;
}

// hard/soft set
void set_ibstart(struct inode *inode, aufs_bindex_t bindex)
{
	struct aufs_iinfo *iinfo = itoii(inode);
	struct inode *h_inode;

	IiMustWriteLock(inode);
	AuDebugOn(sbend(inode->i_sb) < bindex);
	iinfo->ii_bstart = bindex;
	h_inode = iinfo->ii_hinode[bindex + 0].hi_inode;
	if (h_inode)
		au_cpup_igen(inode, h_inode);
}

void set_ibend(struct inode *inode, aufs_bindex_t bindex)
{
	IiMustWriteLock(inode);
	AuDebugOn(sbend(inode->i_sb) < bindex
		  || bindex < ibstart(inode));
	itoii(inode)->ii_bend = bindex;
}

void set_ivdir(struct inode *inode, struct aufs_vdir *vdir)
{
	IiMustWriteLock(inode);
	AuDebugOn(!S_ISDIR(inode->i_mode)
		  || (itoii(inode)->ii_vdir && vdir));
	itoii(inode)->ii_vdir = vdir;
}

void aufs_hiput(struct aufs_hinode *hinode)
{
	au_hin_free(hinode);
	dput(hinode->hi_whdentry);
	iput(hinode->hi_inode);
}

unsigned int au_hi_flags(struct inode *inode, int isdir)
{
	unsigned int flags;
	struct super_block *sb = inode->i_sb;
	struct aufs_sbinfo *sbinfo = stosi(sb);

	flags = 0;
	if (AuFlag(sbinfo, f_xino) != AuXino_NONE)
		flags = AUFS_HI_XINO;
	if (unlikely(isdir && au_flag_test_udba_inotify(sb)))
		flags |= AUFS_HI_NOTIFY;
	return flags;
}

struct aufs_hinode *itohi(struct inode *inode, aufs_bindex_t bindex)
{
	//todo: this lock check causes some unnecessary locks in callers.
	IiMustAnyLock(inode);
	return itoii(inode)->ii_hinode + bindex;
}

void set_h_iptr(struct inode *inode, aufs_bindex_t bindex,
		struct inode *h_inode, unsigned int flags)
{
	struct aufs_hinode *hinode;
	struct inode *hi;
	struct aufs_iinfo *iinfo = itoii(inode);

	LKTRTrace("i%lu, b%d, hi%lu, flags 0x%x\n",
		  inode->i_ino, bindex, h_inode ? h_inode->i_ino : 0, flags);
	IiMustWriteLock(inode);
	hinode = iinfo->ii_hinode + bindex;
	hi = hinode->hi_inode;
	AuDebugOn(bindex < ibstart(inode) || ibend(inode) < bindex
		  || (h_inode && atomic_read(&h_inode->i_count) <= 0)
		  || (h_inode && hi));

	if (hi) {
#if 0 // remove this
		if (unlikely(!hi->i_nlink && (flags & AUFS_HI_XINO))) {
			AuDebugOn(sbr_id(sb, bindex) != hinode->hi_id);
			xino_write0(inode->i_sb, bindex, hi->i_ino, 0);
			/* ignore this error */
			/* bad action? */
		}
#endif
		aufs_hiput(hinode);
	}
	hinode->hi_inode = h_inode;
	if (h_inode) {
		int err;
		struct super_block *sb = inode->i_sb;

		if (bindex == iinfo->ii_bstart)
			au_cpup_igen(inode, h_inode);
		hinode->hi_id = sbr_id(sb, bindex);
		if (flags & AUFS_HI_XINO) {
			struct xino_entry xinoe = {
				.ino	= inode->i_ino,
				//.h_gen	= h_inode->i_generation
			};
			err = xino_write(sb, bindex, h_inode->i_ino, &xinoe);
			if (unlikely(err)) {
				/* bad action? */
				AuIOErr1("failed xino_write() %d,"
					 " force noxino\n",
					 err);
				AuFlagSet(stosi(sb), f_xino, AuXino_NONE);
			}
		}
#ifdef CONFIG_AUFS_HINOTIFY
		if (unlikely((flags & AUFS_HI_NOTIFY)
			     && br_hinotifyable(sbr_perm(sb, bindex)))) {
			err = au_hin_alloc(hinode, inode, h_inode);
			if (unlikely(err))
				AuIOErr1("au_hin_alloc() %d\n", err);
			else
				AuDebugOn(!hinode->hi_notify);
		}
#endif
	}
}

void set_hi_wh(struct inode *inode, aufs_bindex_t bindex, struct dentry *h_wh)
{
	struct aufs_hinode *hinode;

	IiMustWriteLock(inode);
	hinode = itoii(inode)->ii_hinode + bindex;
	AuDebugOn(hinode->hi_whdentry);
	hinode->hi_whdentry = h_wh;
}

void au_update_iigen(struct inode *inode)
{
	//IiMustWriteLock(inode);
	AuDebugOn(!inode->i_sb);
	atomic_set(&itoii(inode)->ii_generation, au_sigen(inode->i_sb));
	//smp_mb(); /* atomic_set */
}

/* it may be called at remount time, too */
void au_update_brange(struct inode *inode, int do_put_zero)
{
	struct aufs_iinfo *iinfo;

	LKTRTrace("i%lu, %d\n", inode->i_ino, do_put_zero);
	IiMustWriteLock(inode);

	iinfo = itoii(inode);
	if (unlikely(!iinfo) || iinfo->ii_bstart < 0)
		return;

	if (do_put_zero) {
		aufs_bindex_t bindex;
		for (bindex = iinfo->ii_bstart; bindex <= iinfo->ii_bend;
		     bindex++) {
			struct inode *h_i;
			h_i = iinfo->ii_hinode[0 + bindex].hi_inode;
			if (h_i && !h_i->i_nlink)
				set_h_iptr(inode, bindex, NULL, 0);
		}
	}

	iinfo->ii_bstart = -1;
	while (++iinfo->ii_bstart <= iinfo->ii_bend)
		if (iinfo->ii_hinode[0 + iinfo->ii_bstart].hi_inode)
			break;
	if (iinfo->ii_bstart > iinfo->ii_bend) {
		iinfo->ii_bstart = -1;
		iinfo->ii_bend = -1;
		return;
	}

	iinfo->ii_bend++;
	while (0 <= --iinfo->ii_bend)
		if (iinfo->ii_hinode[0 + iinfo->ii_bend].hi_inode)
			break;
	AuDebugOn(iinfo->ii_bstart > iinfo->ii_bend || iinfo->ii_bend < 0);
}

/* ---------------------------------------------------------------------- */

int au_iinfo_init(struct inode *inode)
{
	struct aufs_iinfo *iinfo;
	struct super_block *sb;
	int nbr, i;

	sb = inode->i_sb;
	AuDebugOn(!sb);
	iinfo = &(container_of(inode, struct aufs_icntnr, vfs_inode)->iinfo);
	AuDebugOn(iinfo->ii_hinode);
	nbr = sbend(sb) + 1;
	if (unlikely(nbr <= 0))
		nbr = 1;
	iinfo->ii_hinode = kcalloc(nbr, sizeof(*iinfo->ii_hinode), GFP_KERNEL);
	//iinfo->ii_hinode = NULL;
	if (iinfo->ii_hinode) {
		for (i = 0; i < nbr; i++)
			iinfo->ii_hinode[i].hi_id = -1;
		atomic_set(&iinfo->ii_generation, au_sigen(sb));
		//smp_mb(); /* atomic_set */
		rw_init_nolock(&iinfo->ii_rwsem);
		iinfo->ii_bstart = -1;
		iinfo->ii_bend = -1;
		iinfo->ii_vdir = NULL;
		return 0;
	}
	return -ENOMEM;
}

static int au_iinfo_write0(struct super_block *sb, struct aufs_hinode *hinode,
			   ino_t ino)
{
	int err, locked;
	aufs_bindex_t bindex;

	err = 0;
	locked = si_read_trylock(sb, !AuLock_FLUSH); // crucio!
	bindex = find_brindex(sb, hinode->hi_id);
	if (bindex >= 0)
		err = xino_write0(sb, bindex, hinode->hi_inode->i_ino, ino);
	/* error action? */
	if (locked)
		si_read_unlock(sb);
	return err;
}

void au_iinfo_fin(struct inode *inode)
{
	struct aufs_iinfo *iinfo;
	aufs_bindex_t bend;
	struct aufs_hinode *hi;
	struct super_block *sb;
	int unlinked;
	ino_t ino;

	iinfo = itoii(inode);
	/* bad_inode case */
	if (unlikely(!iinfo))
		return;

	if (unlikely(iinfo->ii_vdir))
		au_vdir_free(iinfo->ii_vdir);

	if (iinfo->ii_bstart >= 0) {
		sb = inode->i_sb;
		unlinked = !inode->i_nlink;
		ino = 0;
		if (unlikely(unlinked))
			ino = inode->i_ino;
		hi = iinfo->ii_hinode + iinfo->ii_bstart;
		bend = iinfo->ii_bend;
		while (iinfo->ii_bstart++ <= bend) {
			if (hi->hi_inode) {
				if (unlikely(unlinked
					     || !hi->hi_inode->i_nlink)) {
					au_iinfo_write0(sb, hi, ino);
					/* ignore this error */
					ino = 0;
				}
				aufs_hiput(hi);
			}
			hi++;
		}
		//iinfo->ii_bstart = iinfo->ii_bend = -1;
	}

	kfree(iinfo->ii_hinode);
	//iinfo->ii_hinode = NULL;
}
