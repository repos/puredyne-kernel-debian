
# $Id: local.mk,v 1.49 2008/01/21 04:55:41 sfjro Exp $

########################################
# default values, see ./fs/aufs/Kconfig after 'make kconfig'
# instead of setting 'n', leave it blank when you disable it.
CONFIG_AUFS = m
CONFIG_AUFS_FAKE_DM = y
CONFIG_AUFS_BRANCH_MAX_127 = y
CONFIG_AUFS_BRANCH_MAX_511 =
CONFIG_AUFS_BRANCH_MAX_1023 =
#CONFIG_AUFS_BRANCH_MAX_32767 =
CONFIG_AUFS_SYSAUFS = y
CONFIG_AUFS_HINOTIFY =
CONFIG_AUFS_EXPORT =
CONFIG_AUFS_ROBR =
CONFIG_AUFS_DLGT =
CONFIG_AUFS_RR_SQUASHFS = y
CONFIG_AUFS_SEC_PERM_PATCH =
CONFIG_AUFS_SPLICE_PATCH =
CONFIG_AUFS_PUT_FILP_PATCH =
CONFIG_AUFS_LHASH_PATCH =
CONFIG_AUFS_DENY_WRITE_ACCESS_PATCH =
CONFIG_AUFS_SYSFS_GET_DENTRY_PATCH =
CONFIG_AUFS_KSIZE_PATCH =
CONFIG_AUFS_WORKAROUND_FUSE =
CONFIG_AUFS_DEBUG = y
CONFIG_AUFS_COMPAT =

AUFS_DEF_CONFIG =
-include priv_def.mk

define conf
ifdef $(1)
AUFS_DEF_CONFIG += -D$(1)
endif
endef

$(foreach i, FAKE_DM BRANCH_MAX_127 BRANCH_MAX_511 BRANCH_MAX_1023 \
	BRANCH_MAX_32767 \
	SYSAUFS HINOTIFY EXPORT ROBR DLGT RR_SQUASHFS \
	SPLICE_PATCH LHASH_PATCH SYSFS_GET_DENTRY_PATCH \
	WORKAROUND_FUSE \
	DEBUG COMPAT, \
	$(eval $(call conf,CONFIG_AUFS_$(i))))

ifeq (${CONFIG_AUFS}, m)
AUFS_DEF_CONFIG += -DCONFIG_AUFS_MODULE -UCONFIG_AUFS
# $(foreach i, PUT_FILP_PATCH KSIZE_PATCH, \
# 	$(eval $(call conf,CONFIG_AUFS_$(i))))
ifdef CONFIG_AUFS_SEC_PERM_PATCH
AUFS_DEF_CONFIG += -DCONFIG_AUFS_SEC_PERM_PATCH
endif
ifdef CONFIG_AUFS_DENY_WRITE_ACCESS_PATCH
AUFS_DEF_CONFIG += -DCONFIG_AUFS_DENY_WRITE_ACCESS_PATCH
endif
ifdef CONFIG_AUFS_PUT_FILP_PATCH
AUFS_DEF_CONFIG += -DCONFIG_AUFS_PUT_FILP_PATCH
endif
ifdef CONFIG_AUFS_KSIZE_PATCH
AUFS_DEF_CONFIG += -DCONFIG_AUFS_KSIZE_PATCH
endif
else
AUFS_DEF_CONFIG += -UCONFIG_AUFS_MODULE -DCONFIG_AUFS
endif

EXTRA_CFLAGS += -I ${CURDIR}/include
EXTRA_CFLAGS += ${AUFS_DEF_CONFIG}
EXTRA_CFLAGS += -DLKTRHidePrePath=\"${CURDIR}/fs/aufs\"
export

########################################
# fake top level make

KDIR = /lib/modules/$(shell uname -r)/build
TgtUtil = aufs.5 aufind.sh mount.aufs auplink aulchown umount.aufs
Tgt = aufs.ko ${TgtUtil}

# the environment variables are not inherited since 2.6.23
MAKE += CONFIG_AUFS=${CONFIG_AUFS} \
	AUFS_EXTRA_CFLAGS="$(shell echo ${EXTRA_CFLAGS} | sed -e 's/\"/\\\\\\"/g')"

all: ${Tgt}
FORCE:

########################################

aufs.ko: fs/aufs/aufs.ko
	test ! -e $@ && ln -s $< $@ || :
fs/aufs/aufs.ko: FORCE
#	@echo ${AUFS_DEF_CONFIG}
	${MAKE} -C ${KDIR} M=${CURDIR}/fs/aufs modules

fs/aufs/Kconfig: Kconfig.in
	@cpp -undef -nostdinc -P -I${KDIR}/include $< | \
		sed -e 's/"#"//' -e 's/^[[:space:]]*$$//' | \
		uniq > $@
kconfig: fs/aufs/Kconfig
	@echo copy all ./fs and ./include to your linux kernel source tree.
	@echo add \'obj-\$$\(CONFIG_AUFS\) += aufs/\' to linux/fs/Makefile.
	@echo add \'source \"fs/aufs/Kconfig\"\' to linux/fs/Kconfig.
	@echo then, try \'make menuconfig\' and go to Filesystem menu.
	@echo when you upgrade your kernel source,
	@echo you need to re-run make $@ to re-build fs/aufs/Kconfig.

########################################

clean:
	${MAKE} -C ${KDIR} M=${CURDIR}/fs/aufs $@
	${MAKE} -C util $@
	${RM} ${Tgt}
	find . -type f \( -name '*~' -o -name '.#*[0-9]' \) | xargs -r ${RM}

build_util:
	${MAKE} -C util
${TgtUtil}: build_util
	-test -e $@ || ln -s util/$@ $@

-include priv.mk
